/**
 * Created by alvinyong on 28/6/16.
 */

//Load Express - look inside node_modules directory
var express = require("express");

//Create an instance of Express Application
var app = express();

//------- start of request processing ---------

app.use(function(req, res, next) {
    console.info("incoming request: %s", req.originalUrl);
     next();
 });

var expressstatic = express.static(__dirname + "/public");
console.info(">> %s", typeof expressstatic);

//Serve Static Files from Public Directory
app.use(express.static(__dirname + "/public"));

//Serve files from bower_components
app.use(express.static(__dirname + "/bower_components"));

//In Express, this is called a Middleware - which is a function that handles/processes request
app.use(function(req, res) {
   console.info("File not found in public: %s" + req.originalUrl);
    res.redirect("/error.html");
});

//Set Our Port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3000);

//Start server on port
app.listen(app.get("port"), function() {
    console.info("Application started on port %d", app.get("port"));
});